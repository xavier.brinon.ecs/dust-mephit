import React from 'react';
import {
  Toolbar,
  Accept,
  Skip,
  VoiceMail,
  Close
} from './features/toolbar/Toolbar'

import './App.css'

const App: React.FC = () => {
  return (
    <div className="App">
      <Toolbar >
	<Accept />
	<Skip />
	<VoiceMail />
	<Close />
      </Toolbar>
    </div>
  );
}

export default App;
