import React from 'react';
import { Button } from 'antd'
import {
  CheckCircleTwoTone,
  RightCircleTwoTone,
  PlaySquareTwoTone,
  CloseCircleTwoTone
} from '@ant-design/icons'

export type SizeType = 'small' | 'middle' | 'large' | undefined
interface IToolbar {
  /*
   * Size of the toolbar, all buttons will inherit the same size 
   */
  size?: SizeType
}

const ToolbarContext = React.createContext<SizeType>('large')
ToolbarContext.displayName = 'Toolbar'

export const Toolbar: React.FC<IToolbar> = ({ children, size='large' }) => {
  
  return (
    <ToolbarContext.Provider value={size}>
      { children }
    </ToolbarContext.Provider>
  );
};

export const Accept: React.FC = () => {
  const size = React.useContext(ToolbarContext)
				  
  return (
    <Button
      type="text"
      icon={<CheckCircleTwoTone twoToneColor="#52c41a" />}
      size={size}
      onClick={onClickAccept}
    >Accept</Button>
  )
}

function onClickAccept (evt: React.MouseEvent<HTMLButtonElement>): void {
  console.log({onClickAccept: evt})
}

export const Skip: React.FC = () => {
  const size = React.useContext(ToolbarContext)
  
  return (
    <Button
      type="text"
      icon={<RightCircleTwoTone />}
      size={size}
      onClick={onClickSkip}
  >Skip</Button>
  )
}

function onClickSkip (evt: React.MouseEvent<HTMLButtonElement>): void {
  console.log({onClickSkip: evt})
}

export const VoiceMail: React.FC = () => {
  const size = React.useContext(ToolbarContext)

  return (
    <Button
      type="text"
      icon={<PlaySquareTwoTone />}
      size={size}
      onClick={onClickVoicemail}
>Voicemail</Button>
  )
}

function onClickVoicemail (evt: React.MouseEvent<HTMLButtonElement>): void {
  console.log({onClickVoicemail: evt})
}

export const Close: React.FC = () => {
  const size = React.useContext(ToolbarContext)
  
  return (
    <Button
      type="text"
      icon={<CloseCircleTwoTone twoToneColor="#eb2f96"/>}
      size={size}
      onClick={onClickClose}
    >Close</Button>
  )
}

function onClickClose (evt: React.MouseEvent<HTMLButtonElement>): void {
  console.log({onClickClose: evt})
}